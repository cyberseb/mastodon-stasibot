"""
This script belongs to the Mastodon StasiBot.
It discovers new accounts by crawling the known users API endpoint
on instances known to StasiBot.
"""
import json
import time
import datetime
import requests
import threading
import mysql.connector
from requests.exceptions import HTTPError


def notify_master(master, bot_instance, bot_token, user):
    """ Sends a Toot to the master, notifying her about this user. """
    host_instance = bot_instance
    token = bot_token
    headers = {'Authorization': 'Bearer ' + token}
    data = {'status': master + ' Check out this user: ' + user['url'], 'visibility': 'direct'}
    response = requests.post(
        url='https://' + host_instance + '/api/v1/statuses', data=data, headers=headers)
    if response.status_code == 200:
        print('Toot sent successfully to Master')
    else:
        print('ERROR: Sending toot to my Master did not work :-( ')


def instances_to_crawl():
    """ Select instances worth crawling from the StasiBot database. """
    cnx_local = mysql.connector.connect(user=conf_mysql_user,
                                        password=conf_mysql_passw,
                                        host=conf_mysql_host,
                                        database=conf_mysql_db)
    cursor_local = cnx_local.cursor()
    query_local = "SELECT * FROM instances WHERE status='online' AND total_known_acct > crawled_known_acct;"
    cursor_local.execute(query_local)
    return cursor_local.fetchall()


def crawl_instance(instance_name: str):
    """"
    Crawls an instance for known accounts and
    adds new accounts to the StasiBot database.

    - only crawls 200 accounts at a time due to API limits
    - notifies @seb about new accounts of interest via toot
    """

    cnx_local = mysql.connector.connect(user=conf_mysql_user,
                                        password=conf_mysql_passw,
                                        host=conf_mysql_host,
                                        database=conf_mysql_db)
    cursor_local = cnx_local.cursor()

    # Get instance details from database
    query_local = "SELECT * FROM instances WHERE name = '" + instance_name + "'"
    cursor_local.execute(query_local)
    result_local = cursor_local.fetchall()
    total_known_accounts = result_local[0][3]
    crawled_known_accounts = result_local[0][4] + 1
    instance_last_update = result_local[0][5]

    # Wait until instance API is fully rested
    current_time = datetime.datetime.now()
    if (instance_last_update + datetime.timedelta(minutes=5)) > current_time:
        time_difference = instance_last_update + datetime.timedelta(minutes=5) - current_time
        pause_needed = time_difference.total_seconds()
        print('[' + instance_name + '] Waiting for API to rest up (' + str(pause_needed) + ' seconds)')
        time.sleep(pause_needed)

    # Start crawling this instance
    for x in range(crawled_known_accounts, total_known_accounts):

        # every 200 requests we will give the instance a break
        if (x - crawled_known_accounts >= 100) and (x % 200 == 0):
            print("[" + instance_name +  "] Giving it's API a break...")
            quit()

        # make the next request
        request_url = 'https://' + instance_name + '/api/v1/accounts/' + str(x)
        try:
            response = requests.get(request_url, timeout=3)
        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')
        except Exception as err:
            print(f'Other error occurred: {err}')
        else:
            if response.status_code == 200:
                new_user = json.loads(response.content)
                # catch that NoneType
                if new_user:
                    try:
                        # build mySQL command to add new user to accounts table
                        add_user = ("INSERT INTO accounts "
                                    "(url, json, home_instance, discovery_instance, discovery_id) "
                                    "VALUES (%s, %s, %s, %s, %s)")
                        if '@' in str(new_user['acct']):
                            home_inst = str(new_user['acct']).split('@', 1)[1]
                        else:
                            home_inst = instance_name
                        data_user = (new_user['url'], str(json.dumps(new_user)), home_inst, instance_name, x)
                        # execute mySQL command
                        cursor_local.execute(add_user, data_user)
                        cnx_local.commit()
                        print(new_user['url'] + ' (found on ' + instance_name + ') [' + str(
                            x) + '] has been added to database.')

                        # build mySQL command to update crawled_known_acct for the instance
                        update_instance = "UPDATE instances SET crawled_known_acct=" + str(x) + " WHERE name='" \
                                          + instance_name + "';"
                        # execute mySQL command
                        cursor_local.execute(update_instance)
                        cnx_local.commit()

                        # Does the new user do some infosec stuff?
                        if 'infosec' in str(new_user['note']).lower():
                            notify_master(conf_mastobot_master,
                                          conf_mastobot_instance,
                                          conf_mastobot_token,
                                          new_user)

                    except mysql.connector.Error as err:
                        print(err)
                        pass
            else:
                print('HTTP response status code was: ' + str(response.status_code))
    cnx_local.close()


if __name__ == "__main__":
    """ Main Loop: Crawl known users from all known instances using multiple threads """

    # Load configuration data
    with open('config.json') as json_data_file:
        config_data = json.load(json_data_file)
    # mySQL configuration
    conf_mysql_host = config_data['mysql']['host']
    conf_mysql_user = config_data['mysql']['user']
    conf_mysql_passw = config_data['mysql']['password']
    conf_mysql_db = config_data['mysql']['db']
    # multi-threading configuration
    conf_max_threads = config_data['multi-threading']['max_threads']
    # Mastodon Bot configuration
    conf_mastobot_instance = config_data['mastodon_bot']['host']
    conf_mastobot_token = config_data['mastodon_bot']['token']
    conf_mastobot_master = config_data['mastodon_bot']['master']

    instances = instances_to_crawl()
    if len(instances) > 0:
        for instance in instances:
            curr_instance_name = instance[0]
            t = threading.Thread(name=curr_instance_name, target=crawl_instance, args=(curr_instance_name,))
            t.start()
            print('\n>>> A new thread has been started for ' + curr_instance_name + ' <<<\n')
            if threading.active_count() >= conf_max_threads:
                print('We have reached the max amount of active threads (' + str(
                    threading.active_count()) + '). Waiting for a thread to finish before we start the next one.\n')
            while threading.active_count() >= conf_max_threads:
                pass
    else:
        # Looks like we are done for today
        quit()
