"""
This script belongs to the Mastodon StasiBot.
It discovers new instances in the Mastodon Fediverse
by analyzing the home instance of each known Mastodon account.
"""
import json
import requests
import threading
import mysql.connector
from requests.exceptions import HTTPError


def response_ok(request_url):
    """ Checks whether the URL returns an HTTP status code of 200 - OK """
    try:
        # print('Sending Request: ' + request_url)
        response = requests.get(request_url, timeout=2)
    except HTTPError as http_err:
        # print(f'HTTP error occurred: {http_err}')
        return False
    except Exception as err:
        # print(f'Other error occurred: {err}')
        return False
    else:
        if response.status_code == 200:
            return True


def get_known_accounts(instance_name):
    """ Returns approximate number of known accounts to instance_name """
    x = 1
    request_url = 'https://' + instance_name + '/api/v1/accounts/' + str(x)
    if response_ok(request_url):
        while response_ok(request_url):
            x = x + 10000
            request_url = 'https://' + instance_name + '/api/v1/accounts/' + str(x)
        while not response_ok(request_url):
            x = x - 1000
            request_url = 'https://' + instance_name + '/api/v1/accounts/' + str(x)
        while response_ok(request_url):
            x = x + 200
            request_url = 'https://' + instance_name + '/api/v1/accounts/' + str(x)
        return x
    else:
        return 0


def check_instance(instance_name: str):
    """ Analyzes instance to add meta data and adds it to StasiBot instance table """
    total_known_accounts = get_known_accounts(instance_name)
    print('[' + instance_name + '} Analysis result: approx. ' + str(total_known_accounts) + ' known accounts.')
    if total_known_accounts > 0:
        inst_status = 'online'
    else:
        inst_status = 'offline'
    # build mySQL command
    add_instance = ("INSERT INTO instances "
                    "(name, status, total_known_acct) "
                    "VALUES (%s, %s, %s) "
                    "ON DUPLICATE KEY UPDATE status=%s, total_known_acct=%s;")
    data_instance = (instance_name, inst_status, total_known_accounts, inst_status, total_known_accounts)
    # execute mySQL command
    try:
        local_cnx = mysql.connector.connect(user=conf_mysql_user,
                                            password=conf_mysql_passw,
                                            host=conf_mysql_host,
                                            database=conf_mysql_db)
        local_cursor = local_cnx.cursor()
        local_cursor.execute(add_instance, data_instance)
        local_cnx.commit()
        local_cnx.close()
    except mysql.connector.Error as err:
        print(err)
    quit()


if __name__ == "__main__":
    """ Main Loop: Iterates through a distinct list of home instances from StasiBot accounts table """

    # Load configuration data
    with open('config.json') as json_data_file:
        config_data = json.load(json_data_file)
    # mySQL configuration
    conf_mysql_host = config_data['mysql']['host']
    conf_mysql_user = config_data['mysql']['user']
    conf_mysql_passw = config_data['mysql']['password']
    conf_mysql_db = config_data['mysql']['db']
    # multi-threading configuration
    conf_max_threads = config_data['multi-threading']['max_threads']

    # connect to Stasibot database
    cnx = mysql.connector.connect(user=conf_mysql_user,
                                  password=conf_mysql_passw,
                                  host=conf_mysql_host,
                                  database=conf_mysql_db)
    cursor = cnx.cursor()
    query = "SELECT DISTINCT(home_instance) FROM accounts;"

    # execute mySQL command
    cursor.execute(query)
    result = cursor.fetchall()

    # Iterate through home instances and start a new thread for each instance until max_threads
    for acct_home_inst in result:
        curr_instance_name = acct_home_inst[0]
        t = threading.Thread(name=curr_instance_name, target=check_instance, args=(curr_instance_name,))
        t.start()
        print('\n>>> A new thread has been started for ' + curr_instance_name + ' <<<\n')
        if threading.active_count() >= conf_max_threads:
            print('>>> We have reached the max amount of active threads (' + str(
                threading.active_count()) + '). Waiting for a thread to finish before we start the next one. <<<\n')
        while threading.active_count() >= conf_max_threads:
            pass
    print('\nWe are done :-)')
    quit()
