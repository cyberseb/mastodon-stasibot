CREATE DATABASE `mastodon` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE `instances` (
  `name` varchar(128) NOT NULL,
  `json` json DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `total_known_acct` int(11) DEFAULT '0',
  `crawled_known_acct` int(11) DEFAULT '0',
  `last_update` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `accounts` (
  `url` varchar(256) NOT NULL,
  `json` json DEFAULT NULL,
  `home_instance` varchar(128) DEFAULT NULL,
  `discovery_instance` varchar(128) DEFAULT NULL,
  `discovery_id` int(11) DEFAULT NULL,
  `last_update` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

